'use strict';

/* https://github.com/angular/protractor/blob/master/docs/toc.md */

describe('my app', function() {


  it('should automatically redirect to /toDoListView when location hash/fragment is empty', function() {
    browser.get('index.html');
    expect(browser.getLocationAbsUrl()).toMatch("/toDoListView");
  });


  describe('toDoListView', function() {

    beforeEach(function() {
      browser.get('index.html#/toDoListView');
    });


    it('should render toDoListView when user navigates to /toDoListView', function() {
      expect(element.all(by.css('[ng-view] p')).first().getText()).
        toMatch(/todoList for toDoListView/);
    });

  });


  describe('addItemView', function() {

    beforeEach(function() {
      browser.get('index.html#/addItemView');
    });


    it('should render addItemView when user navigates to /addItemView', function() {
      expect(element.all(by.css('[ng-view] p')).first().getText()).
        toMatch(/addItem for addView/);
    });

  });
});
