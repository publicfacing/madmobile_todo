'use strict';


var app = angular.module('myApp.toDoListView', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/', {
            templateUrl: 'MadMobile_ToDo/app/toDoListView/toDoListView.html',
            controller: 'todoViewController'
        });
    }]);

    app.controller('todoViewController', function($scope) {
        $scope.Value = '';

        $scope.taskList = [
            {item: 'Test Task One'},
            {item: 'Test Task Two'},
            {item: 'Test Task Three'},
            {item: 'Test Task Four'}
        ];

        $scope.addToList = function() {
            $scope.taskList.push( {item: $scope.enteredTask} );
            $scope.enteredTask = '';
        };

        $scope.delete = function(item) {
            var index = $scope.taskList.indexOf(item);
            $scope.taskList.splice(index, 1);
        };

    });