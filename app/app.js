'use strict';

// Declare app level module which depends on views, and components
var app = angular.module('myApp', [
  'ngRoute',
  'myApp.toDoListView',
  'myApp.version'
])
    .config(function($locationProvider, $routeProvider) {
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
        $routeProvider
             .when('/', {
                templateUrl: 'MadMobile_ToDo/app/toDoListView/toDoListView.html',
                controller: 'todoViewController'
            })
            .otherwise({ redirectTo: '/' });
});

//config(['$routeProvider', function($routeProvider) {
//  $routeProvider.otherwise({redirectTo: 'toDoListView/toDoListView'});
//}]);
